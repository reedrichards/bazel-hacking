#include <pybind11/pybind11.h>
#include "native/native.h"

namespace py = pybind11;

PYBIND11_MODULE(binding, m)
{
  m.doc() = "fast inverse square root from quake as a python library";
  m.def("Q_rsqrt", &Q_rsqrt, "estimate 1/sqrt(x)");

}
